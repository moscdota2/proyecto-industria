from wtforms import Form, StringField, SelectField, SubmitField
from wtforms.validators import Length, Required, ValidationError


def field_float(form, campo):
    try:
        float(campo.data)
    except:
        raise ValidationError(
            'No se aceptan otros datos que no sean numericos')


def field_int(form, campo):
    try:
        int(campo.data)
    except:
        raise ValidationError('No se aceptan decimales')


msj = 'Valor requerido'


class IndustriaForm(Form):
    nombre_industria = StringField(
        'Ingrese el nombre de la industria',  validators=[Required(message=msj)])
    letra_rif = SelectField("Ingrese su rif", choices=[(
        "J", "J"), ("G", "G")],  validators=[Required(message=msj)])
    numero_rif = StringField(validators=[Required(message=msj), Length(
        min=9, max=9, message='Longitud del rif erronea'), field_int])
    nombre_planta = StringField("Ingrese el nombre de la planta: ", validators=[
                                Required(message=msj)])
    cantidad_operativa = StringField("Cantidad operativa", validators=[
                                     Required(message=msj), field_float])

    submit = SubmitField('Registrar')


class EliminarRegistroForm(Form):
    rif = StringField("Ingrese el rif", validators=[Required(
        message=msj), Length(min=10, max=10, message='Longitud del rif erronea')])

    submit = SubmitField('Buscar')
