from numpy.lib.arraysetops import isin
import pandas as pd
import numpy as np

from flask import Flask,flash, render_template
from flask_pymongo import PyMongo
# from pymongo import MongoClient

## Conexión a MongoDB ##
def conf_mongo(app):
    app.config["MONGO_URI"] = "mongodb://localhost:27017/industria_test"
    mongo = PyMongo(app)
    db = mongo.db
    
    return db


def validate_columns(data):
    datafun = data

    #################################################################

    #Validación de numeros en primera columna 'Capacidad Operativa'
    if np.dtype(datafun['Capacidad Operativa']) == ('int64' or 'float64'):
        pass
    else:
        war1 = "Cargue con números la columna 'Capacidad Operativa'"
        flash(war1, 'alert alert-warning')
        return render_template('upload_file.html')

    if datafun['Capacidad Operativa'].isnull().values.any():
        war_null_1 = "Cargue los datos de la columna 'Capacidad Operativa'"
        flash(war_null_1, 'alert alert-warning')
        return render_template('upload_file.html')
    else:
        pass

    ##################################################################

    #Validación de numeros en segunda columna 'Producción Planificada'
    if np.dtype(datafun['Producción Planificada']) == ('int64' or 'float64'):
        pass
    else:
        war2 = "Cargue con números la columna 'Producción Planificada'"
        flash(war2, 'alert alert-warning')
        return render_template('upload_file.html')

    if datafun['Producción Planificada'].isnull().values.any():
        war_null_2 = "Cargue los datos de la columna 'Producción Planificada'"
        flash(war_null_2, 'alert alert-warning')
        return render_template('upload_file.html')
    else:
        pass

    ###################################################################

    #Validación de numeros en tercera columna 'Planificación Ejecutada'
    if np.dtype(datafun['Planificación Ejecutada']) in ['float64', 'int64']:
        pass
    else:
        war3 = "Cargue con números la columna 'Planificación Ejecutada'"
        flash(war3, 'alert alert-warning')
        return render_template('upload_file.html')

    if datafun['Planificación Ejecutada'].isnull().values.any():
        war_null_3 = "Cargue los datos de la columna 'Planificación Ejecutada'"
        flash(war_null_3, 'alert alert-warning')
        return render_template('upload_file.html')
    else:
        pass 

    ####################################################################

    #Validación de numeros en cuarta columna 'Producción Comercializada'
    if np.dtype(datafun['Producción Comercializada']) in ['float64', 'int64']:
        pass
    else:
        war4 = "Cargue con números la columna 'Producción Comercializada'"
        flash(war4, 'alert alert-warning')
        return render_template('upload_file.html')

    if datafun['Producción Comercializada'].isnull().values.any():
        war_null_4 = "Cargue los datos de la columna 'Producción Comercializada'"
        flash(war_null_4, 'alert alert-warning')
        return render_template('upload_file.html')
    else:
        pass 

    ######################################################################

    # Validacion de quinta columna 'Nudos Criticos' si son letras
    if np.dtype(datafun['Nudos Criticos']) == 'object':
        pass
    ######################################################################

    #Validación de sexta columna 'RIF'
    if np.dtype(datafun['Rif'])== 'object':
        pass
    else:
        war6 = "Cargue los datos de la columna 'RIF'"
        flash(war6, 'alert alert-warning')
        return render_template('upload_file.html')        

    if datafun['Rif'].isnull().values.any():
        war_null_6 = "Cargue los datos de la columna 'RIF' que están vacios"
        flash(war_null_6, 'alert alert-warning')
        return render_template('upload_file.html')
    else:
        pass

    ######################################################################

    #Validación de septima columna 'Planta'
    if np.dtype(datafun['Planta']) != 'object':
        war7 = "Cargue los datos de la columna 'Planta'"
        flash(war7, 'alert alert-warning')
        return render_template('upload_file.html') 
    
    if datafun['Planta'].isnull().values.any():
        war_null_7 = "Cargue los datos de la columna 'Planta'"
        flash(war_null_7, 'alert alert-warning')
        return render_template('upload_file.html')


def validate_rif(data, db):
    datafun = data
    
    rif_data1 = list(set(datafun['Rif']))

    if len(rif_data1) == 1:
        rif_data = db.industria_validacion.find({'Rif': rif_data1[0]})
    
    for i in rif_data:
        if i:
            return None

    war_rif = 'No se encuentra el RIF registrado en la aplicación'
    flash(war_rif, 'alert alert-danger')
    return render_template('upload_file.html')
        
    



