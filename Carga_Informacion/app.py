import io
import ast
import pandas as pd

from datetime import datetime
from flask import Flask, redirect, url_for, render_template, request, flash, Response
from iteration_utilities import unique_everseen

from form import IndustriaForm, EliminarRegistroForm
from fun import conf_mongo, validate_columns, validate_rif


app = Flask(__name__)
db = conf_mongo(app)


@app.route('/', methods=['GET', 'POST'])


@app.route('/Home', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        return render_template('index.html')

    return render_template('index.html')


# Validaciones
@app.route('/Validaciones_Cargadas', methods=['GET'])
def validation_saved():
    coleccion = db.industria_validacion

    lista_datos = []
    for s in coleccion.find():
        lista_datos.append(
            {'Nombre Industria': s['Nombre_Industria'],
             "Rif": s["Rif"],
             "Nombre_Planta": s["Nombre_Planta"],
             "Capacidad_Operativa": s["Capacidad_Operativa"]}
        )

    conditions = len(lista_datos)
    return render_template('validation_view_info.html', Validaciones=lista_datos, conditions=conditions)


@app.route('/Carga_validacion', methods=['GET', 'POST'])
def upload_validation():
    industria_form = IndustriaForm(request.form)

    if request.method == 'POST' and industria_form.validate():

        nombre_industria = industria_form.nombre_industria.data
        rif_letra = industria_form.letra_rif.data
        resgistro_fiscal = industria_form.numero_rif.data
        nombre_planta = industria_form.nombre_planta.data
        capacidad_operativa = industria_form.cantidad_operativa.data

        db.industria_validacion.insert_one({
            "Nombre_Industria": nombre_industria,
            "Rif": f"{str(rif_letra)}{str(resgistro_fiscal)}",
            "Nombre_Planta": nombre_planta,
            "Capacidad_Operativa": capacidad_operativa})

        return redirect(url_for('validation_saved'))

    return render_template('validation.html', form=industria_form)


# Cargar Archivos
@app.route('/Carga_archivo', methods=['GET', 'POST'])
def upload_file():
    fechas_fechas = datetime.now()
    fechas_mes = fechas_fechas.month
    if fechas_mes < 10:
        fechas_mes = '0'+str(fechas_fechas.month)
        
    fechas_dia = fechas_fechas.day
    if fechas_dia < 10:
        fechas_dia = '0'+str(fechas_fechas.day)

    hora = fechas_fechas.hour
    if hora < 10:
        hora = '0'+str(fechas_fechas.hour)

    minutos = fechas_fechas.minute
    if minutos < 10:
        minutos = '0'+str(fechas_fechas.minute)      
    
    fecha_hoy = f'{fechas_fechas.year}-{fechas_mes}-{fechas_dia}'
    fecha_hoy_hora = f'{hora}:{minutos}'

    if request.method == 'POST':

        if request.form.get('positivo'):

            data_uploat = request.form.get('data_data')
            data = ast.literal_eval(data_uploat)
            db.industria_archivo.insert_many(data)

            success = 'Datos cargados correctamente'
            flash(success, 'alert alert-success')
            return redirect(url_for('home'))

        if request.form.get('negativo'):
            return redirect(url_for('upload_file'))

        file = request.files.get('file_excel')

        if not file:
            danger = 'Cargue el archivo Excel'
            flash(danger, 'alert alert-danger')
            return redirect(url_for('upload_file'))

        try:
            df = pd.read_excel(file, engine='openpyxl')
            df = df.fillna({'Nudos Criticos': ''})
            
            df2 = df.copy()
            
            fecha = f"{request.form.get('fecha_mes')} {request.form.get('fecha_hora')}"
            fecha_sistema = f"{fecha_hoy} {fecha_hoy_hora}"
            df.insert(5, 'Fechas', fecha, allow_duplicates=False)
            df.insert(6, 'Fecha De Sistema', fecha_sistema)
            
            x = validate_columns(df)
            y = validate_rif(df, db)
            
            df_records = df.to_dict('records')
        except:
            
            error_data = 'Archivo invalido'
            flash(error_data, 'alert alert-warning')
            return redirect(url_for('upload_file'))



        if x is None:
            if y is None:
                columnas = df2.columns.tolist()
                data = df2.values.tolist()
                
                return render_template('confirmation.html', columnas=columnas, data=data, df=df_records)

    return render_template('upload_file.html', hora=fecha_hoy_hora, fecha_hoy=fecha_hoy )

# Descargar Plantilla excel
@app.route('/download', methods=['GET', 'POST'])
def excel_template():     
    
    
    buffer = io.BytesIO()
    

    columnas = ['Capacidad Operativa', 'Producción Planificada', 
        'Planificación Ejecutada', 'Producción Comercializada', 
        'Nudos Criticos', 'Rif', 'Planta'] 

    template_excel = pd.DataFrame(columns=columnas)
    template_excel.to_excel(buffer, index = False, header=True)
    print(template_excel)
    
    return Response(
       buffer.getvalue(),
       mimetype="text/xlsx",
       headers={"Content-disposition":
       "attachment; filename=Plantilla_excel.xlsx"})



# Eliminar registros
@app.route('/Eliminar_registro', methods=['GET', 'POST'])
def del_reg():

    eliminar_reg = EliminarRegistroForm(request.form)

    conditions = -1

    if request.method == 'POST' and eliminar_reg.validate():

        rif = eliminar_reg.rif.data.upper()
        coleccion = db.industria_archivo.find({'Rif': rif})

        lista_datos = []
        lista_rif = []

        if coleccion is not None:
            for s in coleccion:
                # lista_datos.append(
                #     {'Planta': s['Planta'],
                #      "Rif": s["Rif"],
                #      "Fecha": s["Fechas"],
                #      "Capacidad Operativa": s['Capacidad Operativa'],
                #      "Producción Planificada": s['Producción Planificada'],
                #      "Planificación Ejecutada": s['Planificación Ejecutada'],
                #      "Producción Comercializada": s['Producción Comercializada'],}
                # )

                lista_datos.append(
                    {"Rif": s["Rif"],
                     "Fecha": s["Fechas"]}
                )
                
                lista_rif.append(
                    {"Fecha": s["Fechas"],
                     "Rif": s["Rif"]})
            

        lista_datos = list(unique_everseen(reversed(lista_datos)))
        conditions = len(lista_datos)

        return render_template('_eliminar_registro.html', form=eliminar_reg, Validaciones=lista_datos[:8], rif=lista_rif[:8], conditions=conditions)

    return render_template('_eliminar_registro.html', form=eliminar_reg, conditions=conditions)


@app.route('/Eliminar_registro/<string:rif>/<string:fecha>', methods=['GET'])
def del_reg_select(rif, fecha):
    
    coleccion = db.industria_archivo.find({'Rif': rif, 'Fechas': fecha})

    for s in coleccion:
        db.industria_archivo.find_one_and_delete({'_id': s['_id']})
        
    success = 'Registro borrado correctamente'
    flash(success, 'alert alert-success')
    return redirect(url_for('del_reg'))


if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    app.run(debug=True)
